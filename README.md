Simple To-do web application which shows what one can do with `EVER` library. `EVER` library is still a part of `libs5` library (`libs5-pjf`, and `libs5-pjf-components`), but it will be moved to a separate project in the near future.

`libs5` libraries one can find on gitlab: [libs5](https://gitlab.com/ahypki/libs5)
