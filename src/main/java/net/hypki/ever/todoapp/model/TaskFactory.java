package net.hypki.ever.todoapp.model;

import java.util.ArrayList;
import java.util.List;

public class TaskFactory {
	
	private static List<Task> tasks = new ArrayList<>();
	
	public static List<Task> getTasks() {
		return tasks;
	}
	
	public static Task getTask(String id) {
		for (Task task : tasks) {
			if (task.getId().equals(id))
				return task;
		}
		return null;
	}
}
