package net.hypki.ever.todoapp.model;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import com.google.gson.annotations.Expose;

public class Task {

	@NotNull
	@NotEmpty
	@Expose
	private String id = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private String text = null;
	
	@NotNull
	@NotEmpty
	@Expose
	private boolean done = false;
	
	public Task() {
		setId(String.valueOf(System.currentTimeMillis()));
		setText("task description");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}
}
