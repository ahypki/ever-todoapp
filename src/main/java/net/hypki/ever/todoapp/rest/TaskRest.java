package net.hypki.ever.todoapp.rest;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import net.hypki.ever.todoapp.model.Task;
import net.hypki.ever.todoapp.model.TaskFactory;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.json.JsonWrapper;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.utils.utils.AssertUtils;
import net.hypki.libs5.utils.utils.ValidationException;

@Path("api/task")
public class TaskRest {

	@POST
	@Path("add")
	@Produces(MediaType.APPLICATION_JSON)
	public String add(@Context SecurityContext sc, 
			@FormParam("text") String text) throws IOException, ValidationException {
		Task newTask = new Task();
		
		if (StringUtilities.notEmpty(text))
			newTask.setText(text);
		
		TaskFactory.getTasks().add(newTask);
		
		return JsonUtils.objectToString(newTask);
	}
	
	@POST
	@Path("toggle")
	@Produces(MediaType.APPLICATION_JSON)
	public String toggle(@Context SecurityContext sc, 
			@FormParam("id") String taskId) throws IOException, ValidationException {
		Task task = TaskFactory.getTask(taskId);
		
		AssertUtils.assertTrue(task != null, "Cannot find task with ID= ", taskId);
		
		task.setDone(!task.isDone());
		
		return JsonUtils.objectToString(task);
	}
}
