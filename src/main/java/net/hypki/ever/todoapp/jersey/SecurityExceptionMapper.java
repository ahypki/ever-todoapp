package net.hypki.ever.todoapp.jersey;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.hypki.libs5.utils.LibsLogger;

@Provider
public class SecurityExceptionMapper implements ExceptionMapper<SecurityException> {

	public Response toResponse(SecurityException exception) {
		try {
			return Response.seeOther(new URI("/login")).build();
		} catch (URISyntaxException e) {
			LibsLogger.error(SecurityExceptionMapper.class, "Cannot redirect", e);
			return Response.status(Response.Status.NOT_ACCEPTABLE).entity("Internal error").build();
		}
	}

}
