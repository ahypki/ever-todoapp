package net.hypki.ever.todoapp.jersey;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import net.hypki.libs5.pjf.op.OpError;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.utils.ValidationException;

@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

	public Response toResponse(ValidationException exception) {
		return Response
				.status(Response.Status.NOT_ACCEPTABLE)
				.entity(new OpList()
								.add(new OpError(exception.toString())).toString())
				.build();
	}

}
