package net.hypki.ever.todoapp.jersey;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import net.hypki.ever.todoapp.EverDemo;

public class JerseyApplication extends ResourceConfig {
 
    public JerseyApplication() {
    	super();
    	
        // Register resources and providers using package-scanning.
    	packages(true, EverDemo.class.getPackage().getName());
        
        register(RolesAllowedDynamicFeature.class);
    }

}
