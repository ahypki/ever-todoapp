package net.hypki.ever.todoapp.ui;

import java.util.UUID;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.utils.string.Base64Utils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.url.UrlUtilities;

import com.google.gson.JsonElement;

public class OpComponentClick extends Op {

	public OpComponentClick(final Component component, final String methodToCall) {
		super("get");
				
		set("url", Component.CLICK_URL);
		set("params", UrlUtilities.toQueryString("componentClass", component.getClass().getName(),
				"componentId", component.getId(),
				"method", methodToCall)
			);
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Component component, final String methodToCall, Params params) {
		super("get");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Component component, final String methodToCall, final JsonElement confirmation, Params params) {
		super("get");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", component.getClass().getName())
			.add("componentId", component.getId())
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("confirmation", confirmation);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + component.getId() + " :input').serialize()")); 
	}

	public OpComponentClick(final Class<? extends Component> componentClass, final String componentId, final String methodToCall, Params params) {
		super("get");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
	
	public OpComponentClick(final Class<? extends Component> componentClass, final String componentId, final String methodToCall, final JsonElement confirmation, Params params) {
		super("get");
		
		if (params == null)
			params = new Params();
		
		params
			.add("componentClass", componentClass.getName())
			.add("componentId", componentId)
			.add("method", methodToCall);
		
		set("url", Component.CLICK_URL);
		set("confirmation", confirmation);
		set("params", params.toString(true));
		set("paramsJS", Base64Utils.encode("$('#" + componentId + " :input').serialize()")); 
	}
}
