package net.hypki.ever.todoapp.ui;

import static org.rendersnake.HtmlAttributesFactory.class_;
import static org.rendersnake.HtmlAttributesFactory.id;
import static org.rendersnake.HtmlAttributesFactory.onClick;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import javax.ws.rs.core.SecurityContext;

import org.rendersnake.HtmlCanvas;

import net.hypki.ever.todoapp.model.Task;
import net.hypki.ever.todoapp.model.TaskFactory;
import net.hypki.ever.todoapp.rest.TaskRest;
import net.hypki.libs5.pjf.op.OpAddClass;
import net.hypki.libs5.pjf.op.OpAppend;
import net.hypki.libs5.pjf.op.OpInfo;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpRemove;
import net.hypki.libs5.pjf.op.OpRemoveClass;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.ValidationException;

public class TasksPanel extends Component {

	public TasksPanel() {
		
	}
	
	private OpList onTaskToggle() throws ValidationException, IOException {
		final String taskId = getParams().getString("taskId");
		
		final String taskString = new TaskRest().toggle(getSecurityContext(), taskId);
		
		final Task task = JsonUtils.fromJson(taskString, Task.class);
		
		return new OpList()
			.add(task.isDone() ? new OpAddClass("#" + task.getId() + " .text", "done") 
								: new OpRemoveClass("#" + task.getId() + " .text", "done"))
			.add(new OpInfo("Task saved"));
	}
	
	private OpList onAddTask() throws IOException {
		final String taskString = new TaskRest().add(getSecurityContext(), null);
		
		Task newTask = JsonUtils.fromJson(taskString, Task.class);
		
		return new OpList()
			.add(new OpAppend("#" + getId() + " .tasks-table", buildRow(new HtmlCanvas(), newTask).toHtml()));
	}
	
	private OpList onRemove() {
		return new OpList()
					.add(new OpRemove("#" + getParams().getString("taskId")));
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		html
			.div(id(getId()))
				.h1()
					.content("To-do app")
				.button(onClick(new OpComponentClick(this, "onAddTask").toStringOnclick())
						.class_("btn btn-default"))
					.content("New task")
			
				.table(class_("table tasks-table"));
		
		for (Task task : TaskFactory.getTasks()) {
			buildRow(html, task);
		}
					
		html
				._table()
			._div()
			;
	}

	private HtmlCanvas buildRow(HtmlCanvas html, Task task) throws IOException {
		return html
			.tr(id(task.getId()))
				.td()
					.input(type("checkbox")
							.onChange(new OpComponentClick(TasksPanel.this, 
									"onTaskToggle", 
									new Params()
										.add("taskId", task.getId())).toStringOnclick())
							.checked(task.isDone() ? "checked" : null))
				._td()
				.td(class_("text " + (task.isDone() ? "done" : "")))
					.content(task.getText())
				.td()
					.button(class_("btn btn-default btn-xs")
							.onClick(new OpComponentClick(TasksPanel.this, 
									"onRemove", 
									new Params()
										.add("taskId", task.getId())).toStringOnclick()))
						.content("Remove")
				._td()
			._tr();
	}
}
