package net.hypki.ever.todoapp.ui;

import java.io.IOException;

import javax.annotation.security.PermitAll;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.pjf.op.OpSet;

@Path("view")
@PermitAll
public class InitHomePage {
	
	@POST
    @Path("home")
	@PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    public String init(@Context SecurityContext sc, @Context UriInfo uriInfo) throws IOException {
		
		return new OpList()
			.add(new OpSet(".jumbotron .container", new TasksPanel().toHtml()))
			.toString();
	}
}
