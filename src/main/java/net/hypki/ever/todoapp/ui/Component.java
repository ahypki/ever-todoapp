package net.hypki.ever.todoapp.ui;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import net.hypki.libs5.pjf.op.Op;
import net.hypki.libs5.pjf.op.OpList;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.reflection.ReflectionUtility;
import net.hypki.libs5.utils.url.Params;
import net.hypki.libs5.utils.utils.ValidationException;

import org.apache.commons.lang.NotImplementedException;
import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

@Path(Component.PATH)
public class Component implements Renderable {

	public static final String PATH = "view/component";
	
	public static final String CLICK = "click";
	public static final String CLICK_URL = PATH + "/" + CLICK;
		
	private String id = null;
	
	private SecurityContext securityContext = null;
	
	private Params params = null;
	
	public Component() {
		
	}
	
	@Override
	public void renderOn(HtmlCanvas html) throws IOException {
		throw new NotImplementedException();
	}
	
	@GET
	@Path(CLICK)
	@Produces(MediaType.APPLICATION_JSON)
	public String click(@Context SecurityContext sc,
			@Context UriInfo uriInfo,
			@QueryParam("componentClass") String componentClass,
			@QueryParam("componentId") String componentId,
			@QueryParam("method") String method) throws IOException, ValidationException {
		
		String queryParams = uriInfo.getRequestUri().getQuery();

		LibsLogger.debug(Component.class, "Click on component ", componentClass, ".", method, "() with ID ", componentId, " with query params ", queryParams);
		
		OpList opList = new OpList();
		
		try {			
			Component component = (Component) Class.forName(componentClass).newInstance();
			component.setId(componentId);
			
			Params additionalParams = new Params(queryParams);
			
			component.setSecurityContext(sc);
			component.setParams(additionalParams);
									
			Object ret = ReflectionUtility.invokeMethod(component, method, new Object[] {sc, additionalParams}, true, true, true);
			LibsLogger.trace(Component.class, "Method ", method, " executed for component ", component.getClass().getSimpleName());
			
			if (ret instanceof Op)
				opList.add((Op) ret);
			else if (ret instanceof OpList)
				opList.add((OpList) ret);
		} catch (Exception e) {
			LibsLogger.error(Component.class, "Cannot call component " + componentClass, e);
		}
		
		return opList.toString();
	}

	public Component setParams(Params params) {
		this.params = params;
		return this;
	}

	public Params getParams() {
		if (params == null)
			params = new Params();
		return params;
	}
	
	public String toHtml() throws IOException {
		return new HtmlCanvas().render(this).toHtml();
	}
	
	public String getId() {
		if (id == null)
			id = String.valueOf(System.currentTimeMillis());
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SecurityContext getSecurityContext() {
		return securityContext;
	}

	protected void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}
}
