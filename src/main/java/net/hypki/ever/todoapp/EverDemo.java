package net.hypki.ever.todoapp;

import static net.hypki.libs5.utils.args.ArgsUtils.exists;
import static net.hypki.libs5.utils.args.ArgsUtils.getInt;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import net.hypki.ever.todoapp.jersey.JerseyApplication;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.args.ArgsUtils;

public class EverDemo {
	
	public static void main(String[] args) throws Exception {	
		// port number for jetty
		final int port = getInt(args, "port", 12000);
		
		// verbose mode
		System.setProperty("log4j", exists(args, "verbose") 
				? "log4j-ever-verbose.properties" : "log4j-ever.properties" );
		
//		System.setErr(System.out);
		
		LibsLogger.debug(EverDemo.class, "Starting Op Demo App on port ", port);
		
		ResourceConfig rc = new JerseyApplication();
		URI baseUri = UriBuilder.fromUri("http://localhost/").port(port).build();
		Server server = JettyHttpContainerFactory.createServer(baseUri, rc);
				
		try {
			server.start();
			
			LibsLogger.info(EverDemo.class, "");
			LibsLogger.info(EverDemo.class, "\tEVER To-Do demo app started on " + baseUri);
			LibsLogger.info(EverDemo.class, "");
			
			server.join();
		} finally {
			server.destroy();
		}
	}
}
